TS脚手架中的文件变化
1. package.json 中多了@types/包，包是个 vscode 识别，提供类型提示
2. js 中不区分 js 文件和 JSX 文件。组件都写在.tsx 文件中，非组件代码写在.ts 文件中
   // TSX 中可以写 JSX 语法
   // TS 中不允许写 JSX 语法
3. 配置文件是 tsconfig.json，tsconfig 和 jsconfig 在脚手架中只能有一个
4. react-app-env.d.ts 是脚手架自动生成的类型文件，作用：给你同 Node、React ReactDOM 语法提示


