import React from 'react';
import { Route, RouteProps } from 'react-router-dom';
// 1. 提取到单独的文件
export interface KeepAliveProps extends RouteProps {
  activePath: string;
  component: any;
}

export function KeepAlive({ activePath, component: Component, ...restProps }: KeepAliveProps) {
  return (
    <Route
      {...restProps}
      children={(_props) => {
        const { pathname } = _props.location;

        const isMatch = pathname.startsWith(activePath);
        return (
          <div style={{ display: isMatch ? 'block' : 'none' }}>
            <Component {..._props} />
          </div>
        );
      }}
    ></Route>
  );
}
