

import React, { ReactNode } from 'react'
import styles from './index.module.scss'
import classNames from 'classnames'

interface IInputProps
  extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  className?: string
  extra?: ReactNode
  errorMsg?: string
}


export default function Input(props: IInputProps) {
  const { className, extra, errorMsg, ...restProps } = props;
  return (
    <div className={classNames(styles.root, className)}>
      <div className='input-item'>
        <div className='input-box'>
          <input
            className='input'
            {...restProps}
          />
          <div className='validate'>{errorMsg}</div>
          <div className='extra'>{extra}</div>
        </div>
      </div>
    </div>
  )
}
