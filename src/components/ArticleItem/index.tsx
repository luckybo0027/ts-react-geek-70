import classnames from 'classnames';
import styles from './index.module.scss';
import { ResultType } from '@/store/reducer/search';
import { useHistory } from 'react-router-dom';
import { Image } from 'antd-mobile';
import imgSrc from '@/assets/none.png';
import { ArticleType } from '@/store/reducer/article';
type ArticleItemType = {
  itemData: ResultType | ArticleType;
};

export default function ArticleItem(props: ArticleItemType) {
  const item = props.itemData;
  const { cover } = item;
  const { images = [], type } = cover || {};
  const history = useHistory();
  return (
    <div className={styles.root} onClick={() => history.push('/article/' + item.art_id)}>
      <div
        className={classnames('article-content', {
          t3: type === 3,
          'none-mt': type === 0,
        })}
      >
        <h3>{item.title}</h3>
        {type !== 0 && (
          <div className="article-imgs">
            {images.map((item, index) => {
              return (
                // 💥1.url可能会重复，脏数据的问题，使用item+index生成一个唯一的值作为key
                <div className="article-img-wrapper" key={item + index}>
                  <Image src={item} fit="cover" lazy fallback={<img src={imgSrc} alt="" />} />
                </div>
              );
            })}
          </div>
        )}
      </div>
      <div className={classnames('article-info', type === 0 ? 'none-mt' : '')}>
        <span>{item.aut_name}</span>
        <span>{item.comm_count}评论</span>
        <span>{item.pubdate}</span>
      </div>
    </div>
  );
}
