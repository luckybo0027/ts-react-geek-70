/*
  学习目标：组件封装总结
  步骤：
     1. 一般组件要支持style与className，👍方便组件自定义样式
     2. 考虑联合字面量+字符串，提供代码提示
     3. 复用React或第三方的类型，快速获得代码提示
     4. 🔔自己写代码的技巧：TSX中封装组件，一般需要先定义props类型，后使用。类似vue中的props
  组件封装要考虑的（三种）四种状态
*/

import React from 'react';
import classnames from 'classnames';

type IconProps = {
  type: IconType;
  className?: string;
  onClick?: () => void;
  style?: React.CSSProperties;
  [key: string]: any;
};
export default function Icon(props: IconProps) {
  const { type, onClick, style, className, ...restProps } = props;

  const cls = classnames('icon', className);
  return (
    // 2.2 修复div为span标签
    <span>
      <svg
        className={cls}
        aria-hidden="true"
        onClick={props.onClick}
        style={props.style}
        {...restProps}
      >
        <use xlinkHref={`#${props.type}`}></use>
      </svg>
    </span>
  );
}

type IconType =
  | 'iconphoto-fail'
  | 'iconphoto'
  | 'iconbtn_right'
  | 'iconicon_unenjoy1'
  | 'iconicon_feedback1'
  | 'iconicon_upload'
  | 'iconbianji'
  | 'icongengduo'
  | 'iconfanhui'
  | 'iconbtn_history1'
  | 'iconbtn_readingtime'
  | 'iconbtn_like2'
  | 'iconbtn_pic'
  | 'iconbtn_mine'
  | 'iconbtn_channel'
  | 'iconbtn_channel_close'
  | 'iconbtn_comment'
  | 'iconbtn_home_sel'
  | 'iconbtn_collect_sel'
  | 'iconbtn_mine_sel'
  | 'iconbtn_collect'
  | 'iconbtn_qa_sel'
  | 'iconbtn_like_sel'
  | 'iconbtn_feedback'
  | 'iconbtn_del'
  | 'iconbtn_tag_close'
  | 'iconbtn_essay_close'
  | 'iconbtn_qa'
  | 'iconbtn_myworks'
  | 'iconicon_blacklist'
  | 'iconbtn_mycollect'
  | 'iconbtn_video_sel'
  | 'iconbtn_share'
  | 'iconbtn_mymessages'
  | 'iconbtn_search'
  | 'iconbtn_like'
  | 'iconbtn_xiaozhitongxue'
  | 'iconbtn_video'
  | 'iconbtn_home';
