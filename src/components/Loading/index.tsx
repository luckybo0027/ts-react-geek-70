import { SpinLoading } from 'antd-mobile'
import React from 'react'
import styles from './index.module.scss'

export default function MyLoading() {
  return (
    <div className={styles.root}>
      <SpinLoading color='primary' ></SpinLoading>
    </div>
  )
}
