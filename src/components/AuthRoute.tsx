import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { hasToekn } from '../utils/storage';
interface AuthRouteProps extends RouteProps {
  component: React.FC;
  path: string;
}

export type LocationStateType = {
  fromPath: string;
};

export default function AuthRoute(props: AuthRouteProps) {
  const { path, component: Component, ...restProps } = props;
  return (
    <Route
      {...restProps}
      path={path}
      render={(_props: any) => {
        if (hasToekn()) {
          return <Component {..._props} />;
        }
        return (
          <Redirect
            to={{
              pathname: '/login',
              state: {
                fromPath: path,
              },
            }}
          />
        );
      }}
    />
  );
}
