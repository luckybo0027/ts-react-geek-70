import Icon from '@/components/Icon';
import classNames from 'classnames';
import { ReactNode } from 'react';
import { useHistory } from 'react-router-dom';
import styles from './index.module.scss';

type NavBarProps = {
  children?: ReactNode;
  right?: ReactNode;
  onBack?: () => void;
  fixed?: boolean;
};
export default function NavBar(props: NavBarProps) {
  const { children, right, onBack, fixed } = props;
  const history = useHistory();
  const handleBack = () => {
    if (onBack) return onBack();
    history.go(-1);
  };
  return (
    <div className={styles.root}>
      <div className={classNames('main', { fixed })}>
        <div className="left" onClick={handleBack}>
          <Icon type="iconfanhui" />
        </div>
        <div className="title">{children}</div>
        <div className="right">{right}</div>
      </div>
    </div>
  );
}
