import classNames from 'classnames';
import React, { useEffect, useRef } from 'react';
import styles from './index.module.scss';

export interface TextAreaProps
  extends React.DetailedHTMLProps<
    React.TextareaHTMLAttributes<HTMLTextAreaElement>,
    HTMLTextAreaElement
  > {
  value?: string;
}

export function TextArea(props: TextAreaProps) {
  const { maxLength = 100, value = '', autoFocus = true, className, ...restProps } = props;

  const textRef = useRef<HTMLTextAreaElement>(null);

  useEffect(() => {
    textRef.current?.setSelectionRange(-1, -1);
  }, []);

  return (
    <div className={styles.root}>
      <textarea
        ref={textRef}
        className={classNames('textarea', className)}
        maxLength={maxLength}
        value={value}
        autoFocus={autoFocus}
        {...restProps}
      />
      <div className="count">
        {value.length}/{maxLength}
      </div>
    </div>
  );
}
