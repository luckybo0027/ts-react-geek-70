import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import store from '@/store';
import './assets/styles/index.scss';

import RelativeTime from 'dayjs/plugin/relativeTime.js';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn.js';
dayjs.locale('zh-cn');
dayjs.extend(RelativeTime);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
