import requset from '@/utils/requset';

export const sendCodeAPI = (mobile: string) => {
  return requset({
    url: '/v1_0/sms/codes/' + mobile,
  });
};

export type LoginFormType = { mobile: string; code: string };
export const loginAPI = (data: LoginFormType) => {
  return requset({
    url: '/v1_0/authorizations',
    method: 'post',
    data,
  });
};
