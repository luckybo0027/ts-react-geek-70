import request from '@/utils/requset';
export const getArtilesByIdAPI = ({ channel_id = 0, timestamp = Date.now() }) => {
  return request({
    url: '/v1_0/articles',
    params: {
      channel_id,
      timestamp,
    },
  });
};
