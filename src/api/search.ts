import requset from '@/utils/requset';
export const getSuggestsAPI = (q: string) => {
  return requset({
    url: '/v1_0/suggestion',
    params: { q },
  });
};

export const getResultsAPI = ({ page = 1, per_page = 10, q = '' }) => {
  return requset({
    url: '/v1_0/search',
    params: { page, per_page, q },
  });
};
