import request from '@/utils/requset';
export type CommentParasm = {
  target: string;
  content: string;
  art_id?: string;
};
export const submitCommentAPI = (data: CommentParasm) => {
  return request({
    url: '/v1_0/comments',
    method: 'post',
    data,
  });
};

export const likeCommentAPI = (id: string) => {
  return request({
    url: '/v1_0/comment/likings',
    method: 'post',
    data: {
      target: id,
    },
  });
};

export const dislikeCommentAPI = (id: string) => {
  return request({
    url: '/v1_0/comment/likings/' + id,
    method: 'delete',
  });
};
