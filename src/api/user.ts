import { ProfileType } from '@/store/reducer/user';
import requset from '@/utils/requset';

export const getUserInfoAPI = () => {
  return requset({
    url: '/v1_0/user',
  });
};

export const getUserProfileAPI = () => {
  return requset({
    url: '/v1_0/user/profile',
  });
};

export const updatePhotoAPI = (data: FormData) => {
  return requset({
    url: '/v1_0/user/photo',
    method: 'patch',
    data,
  });
};

export const updateProfileAPI = (data: Partial<ProfileType>) => {
  return requset({
    url: '/v1_0/user/profile',
    method: 'patch',
    data,
  });
};
