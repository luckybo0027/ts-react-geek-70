import request from '@/utils/requset';
export const getDetailByIdAPI = (id: string) => {
  return request({
    url: '/v1_0/articles/' + id,
  });
};

export type ParamsType = {
  type: 'a' | 'c';
  source: string;
};

export const getCommentsByIdAPI = (params: ParamsType) => {
  return request({
    url: '/v1_0/comments',
    params,
  });
};
