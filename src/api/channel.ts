import { ChannelType } from '@/store/reducer/channel';
import requset from '@/utils/requset';

export const getChannelListAPI = () => {
  return requset({
    url: '/v1_0/channels',
  });
};

export const getuserChannelListAPI = () => {
  return requset({
    url: '/v1_0/user/channels',
  });
};

export const delUserChannelByIdAPI = (id: number) => {
  return requset({
    url: '/v1_0/user/channels/' + id,
    method: 'DELETE',
  });
};

export const addUserChannelByIdAPI = (channelList: ChannelType[]) => {
  return requset({
    url: '/v1_0/user/channels',
    method: 'PATCH',
    data: {
      channels: channelList,
    },
  });
};
