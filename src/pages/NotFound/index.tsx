import { Button, ErrorBlock, Space } from 'antd-mobile'
import React from 'react'
import { useHistory } from 'react-router-dom'

export default function NotFound() {
  const history = useHistory()
  return (
    <div>
      <ErrorBlock status='empty' fullPage>
        <Space>
          <Button color="primary" onClick={() => history.push('/')}>返回首页</Button>
          <Button onClick={() => history.push('/')}>返回首页</Button>
        </Space>
      </ErrorBlock>
    </div>
  )
}
