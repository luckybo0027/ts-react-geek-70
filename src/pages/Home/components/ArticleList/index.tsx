import ArticleItem from '@/components/ArticleItem';
import { useAppSelector } from '@/store';
import { getArtilesByIdAction, updateArtilesByIdAction } from '@/store/action/article';
import { InfiniteScroll, PullToRefresh } from 'antd-mobile';
import { useDispatch } from 'react-redux';

// 3. 删除props
export default function ArticleList() {
  const dispatch = useDispatch();
  const { currentId } = useAppSelector((state) => state.channel);
  const { articleList } = useAppSelector((state) => state.article);
  const pre_timestamp = articleList[currentId]?.pre_timestamp;

  const list = articleList[currentId]?.results || [];
  const loadMore = async () => {
    await dispatch(updateArtilesByIdAction({ channel_id: currentId, timestamp: pre_timestamp }));
  };

  const handleRefresh = async () => {
    await dispatch(getArtilesByIdAction({ channel_id: currentId }));
  };
  return (
    <PullToRefresh onRefresh={handleRefresh}>
      <div>
        {list.map((item) => {
          return <ArticleItem itemData={item as any} key={item.art_id} />;
        })}

        {!!list.length && <InfiniteScroll loadMore={loadMore} hasMore={!!pre_timestamp} />}
      </div>
    </PullToRefresh>
  );
}
