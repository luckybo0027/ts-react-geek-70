import ArticleList from '@/pages/Home/components/ArticleList';
import Channel from '@/components/Channel';
import React, { useEffect } from 'react';

import styles from './index.module.scss';
import { getArtilesByIdAction } from '@/store/action/article';
import { useDispatch } from 'react-redux';
import { useAppSelector } from '@/store';

export default function Home() {
  const dispatch = useDispatch();
  const { currentId, userList } = useAppSelector((state) => state.channel);

  const { articleList } = useAppSelector((state) => state.article);
  const list = articleList[currentId]?.results || [];
  useEffect(() => {
    if (list.length) {
      return;
    }
    dispatch(getArtilesByIdAction({ channel_id: currentId }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, currentId]);
  return (
    <div className={styles.root}>
      <Channel></Channel>
      {userList.map((item) => {
        return (
          <div
            key={item.id}
            className="channel-list"
            style={{ display: currentId === item.id ? 'block' : 'none' }}
          >
            <ArticleList />
          </div>
        );
      })}
    </div>
  );
}
