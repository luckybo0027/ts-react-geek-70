import Icon from '@/components/Icon';
import { useAppSelector } from '@/store';
import {
  likeCommentsAction,
  likeOriginCommentsAction,
  likeReplyCommentsAction,
  saveOriginCommentAction,
} from '@/store/action/comment';
import { CommentType } from '@/store/reducer/comment';
import dayjs from 'dayjs';
import { useDispatch } from 'react-redux';
import styles from './index.module.scss';

type Props = {
  comment: CommentType;

  type?: 'normal' | 'reply';
};

export default function CommentItem({ comment, type = 'normal' }: Props) {
  const dispatch = useDispatch();
  const { originComment } = useAppSelector((state) => state.comment);
  const handleLike = async () => {
    if (originComment.com_id === comment.com_id) {
      await dispatch(likeOriginCommentsAction(comment.com_id, comment.is_liking));
      return;
    }
    if (originComment.com_id) {
      await dispatch(likeReplyCommentsAction(comment.com_id, comment.is_liking));
      return;
    }

    await dispatch(likeCommentsAction(comment.com_id, comment.is_liking));
  };

  return (
    <div className={styles.root}>
      {/* 评论者头像 */}
      <div className="avatar">
        <img src={comment.aut_photo} alt="" />
      </div>

      <div className="comment-info">
        {/* 评论者名字 */}
        <div className="comment-info-header">
          <span className="name">{comment.aut_name}</span>

          {/* 关注或点赞按钮 */}
          <span className="thumbs-up" onClick={handleLike}>
            {comment.like_count}
            <Icon type={comment.is_liking ? 'iconbtn_like_sel' : 'iconbtn_like2'} />
          </span>
        </div>

        {/* 评论内容 */}
        <div className="comment-content">{comment.content}</div>

        <div className="comment-footer">
          {/* 回复按钮 */}
          {type === 'normal' && (
            <span className="replay" onClick={() => dispatch(saveOriginCommentAction(comment))}>
              {comment.reply_count}回复 <Icon type="iconbtn_right" />
            </span>
          )}

          {/* 评论日期 */}
          <span className="comment-time">{dayjs(comment.pubdate).format('YYYY-MM-DD')}</span>
        </div>
      </div>
    </div>
  );
}
