import { useAppSelector } from '@/store';
import {
  clearOriginCommentAction,
  clearReplyCommentsAction,
  getReplyCommentsByIdAction,
} from '@/store/action/comment';
import { NavBar } from 'antd-mobile';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import CommentFooter from '../CommentFooter';
import CommentItem from '../CommentItem';
import NoComment from '../NoComment';
import styles from './index.module.scss';

export default function CommentReply() {
  const { originComment, replyComments } = useAppSelector((state) => state.comment);
  const { results } = replyComments;
  const dispatch = useDispatch();
  const handleBack = () => {
    dispatch(clearOriginCommentAction());
    dispatch(clearReplyCommentsAction());
  };

  useEffect(() => {
    dispatch(getReplyCommentsByIdAction(originComment.com_id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  return (
    <div className={styles.root}>
      <div className="reply-wrapper">
        {/* 顶部导航栏 */}
        <NavBar className="transparent-navbar" onBack={handleBack}>
          <div>{results.length}条回复</div>
        </NavBar>

        {/* 原评论信息 */}
        <div className="origin-comment">
          <CommentItem type="reply" comment={originComment}></CommentItem>
        </div>

        {/* 回复评论的列表 */}
        <div className="reply-list">
          <div className="reply-header">全部回复</div>
          {!results.length ? (
            <NoComment />
          ) : (
            results.map((item) => <CommentItem type="reply" comment={item} key={item.com_id} />)
          )}
        </div>

        {/* 评论工具栏，设置 type="reply" 不显示评论和点赞按钮 */}
        <CommentFooter type="reply" />
      </div>
    </div>
  );
}
