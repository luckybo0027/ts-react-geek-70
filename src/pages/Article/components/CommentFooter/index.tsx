import Icon from '@/components/Icon';
import { useAppSelector } from '@/store';
import { ArticleDetail } from '@/store/reducer/article';
import { Popup } from 'antd-mobile';
import { useState } from 'react';
import CommentInput from '../CommentInput';
import styles from './index.module.scss';

type CommentFooterProps = {
  articleData?: ArticleDetail;

  type?: 'normal' | 'reply';

  onClick?: () => void;

  onShare?: () => void;
};

export default function CommentFooter({
  type = 'normal',
  articleData = {} as ArticleDetail,
  onClick,
  onShare,
}: CommentFooterProps) {
  const [visible, setVisible] = useState(false);
  const { originComment } = useAppSelector((state) => state.comment);

  return (
    <div className={styles.root}>
      <div className="comment-footer">
        <div className="input-btn" onClick={() => setVisible(true)}>
          <Icon type="iconbianji" />
          <span>去评论</span>
        </div>

        {type === 'normal' && (
          <>
            <div className="action-item" onClick={onClick}>
              <Icon type="iconbtn_comment" />
              <p>评论</p>
              <span className="bage">{articleData.comm_count}</span>
            </div>
            <div className="action-item">
              <Icon type={articleData.attitude === 1 ? 'iconbtn_like_sel' : 'iconbtn_like2'} />
              <p>点赞</p>
            </div>
          </>
        )}
        <div className="action-item">
          <Icon type={articleData.is_collected ? 'iconbtn_collect_sel' : 'iconbtn_collect'} />
          <p>收藏</p>
        </div>
        <div className="action-item" onClick={onShare}>
          <Icon type="iconbtn_share" />
          <p>分享</p>
        </div>
      </div>
      <Popup destroyOnClose visible={visible} bodyStyle={{ height: '100vh' }}>
        <CommentInput name={originComment.aut_name} onBack={() => setVisible(false)} />
      </Popup>
    </div>
  );
}
