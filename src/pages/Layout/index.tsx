import AuthRoute from '@/components/AuthRoute';
import Icon from '@/components/Icon';
import MyLoading from '@/components/Loading';
import { TabBar } from 'antd-mobile';
import path from 'path';
import React from 'react';
import { Route, RouteProps, Switch, useHistory, useLocation } from 'react-router-dom';

import styles from './index.module.scss';
import { KeepAlive } from '../../components/KeepAlive';

const Home = React.lazy(() => import('@/pages/Home'));
const NotFound = React.lazy(() => import('@/pages/NotFound'));
const Profile = React.lazy(() => import('@/pages/Profile'));
const QA = React.lazy(() => import('@/pages/QA'));
const Video = React.lazy(() => import('@/pages/Video'));

export default function Layout() {
  const histroy = useHistory();
  const tabList = [
    {
      key: '/layout/home',
      icon: (active: boolean) => <Icon type={active ? 'iconbtn_home_sel' : 'iconbtn_home'} />,
      title: '首页',
    },
    {
      key: '/layout/qa',
      icon: (active: boolean) => <Icon type={active ? 'iconbtn_qa_sel' : 'iconbtn_qa'} />,
      title: '问答',
    },
    {
      key: '/layout/video',
      icon: (active: boolean) => <Icon type={active ? 'iconbtn_video_sel' : 'iconbtn_video'} />,
      title: '视频',
    },
    {
      key: '/layout/profile',
      icon: (active: boolean) => <Icon type={active ? 'iconbtn_mine_sel' : 'iconbtn_mine'} />,
      title: '我的',
    },
  ];

  const { pathname } = useLocation();

  return (
    <div className={styles.root}>
      <div className="tab-content">
        <React.Suspense fallback={<MyLoading />}>
          <KeepAlive activePath="/layout/home" component={Home}></KeepAlive>
          <Switch>
            <Route path="/layout/qa" component={QA} />
            <Route path="/layout/video" component={Video} />
            <AuthRoute path="/layout/profile" component={Profile} />
            <Route
              render={(props) => {
                const isLayout = props.location.pathname.startsWith('/layout');
                if (isLayout) return null;
                return <NotFound />;
              }}
            />
          </Switch>
        </React.Suspense>
      </div>

      <TabBar
        activeKey={pathname}
        className="geek-tabbar"
        onChange={(key: string) => histroy.push(key)}
      >
        {tabList.map((item) => (
          <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
        ))}
      </TabBar>
    </div>
  );
}
