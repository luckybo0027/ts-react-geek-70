import { Button } from 'antd-mobile';
import React from 'react';
import { useHistory } from 'react-router-dom';

export default function Test() {
  const history = useHistory();

  return (
    <div style={{ padding: 24, backgroundColor: '#ccc' }}>
      <Button onClick={() => history.push('/')}>点我</Button>
    </div>
  );
}
