import { getLocalHistories } from '@/utils/storage';
import { uniq } from 'lodash';

const initialState = {
  suggests: [] as string[],
  histories: getLocalHistories(),
  resultData: {
    per_page: 10,
    page: 1,
    results: [] as ResultType[],
    total_count: 0,
  },
};

export type ResultType = {
  art_id: string;
  title: string;
  aut_id: string;
  aut_name: string;
  comm_count: number;
  pubdate: string;
  cover: {
    type: number;
    images: string[];
  };
  like_count: number;
  collect_count: number;
};

type ActionType =
  | { type: 'search/saveSuggests'; payload: string[] }
  | { type: 'search/saveHistories'; payload: string }
  | { type: 'search/clearHistories' }
  | { type: 'search/saveResults'; payload: typeof initialState.resultData }
  | { type: 'search/updateResults'; payload: typeof initialState.resultData };

export default function searchReducer(
  state = initialState,
  action: ActionType
): typeof initialState {
  switch (action.type) {
    case 'search/saveSuggests':
      return {
        ...state,
        suggests: action.payload.filter((item) => !!item),
      };

    case 'search/saveHistories':
      return {
        ...state,

        histories: uniq([action.payload, ...state.histories]).slice(0, 10),
      };

    case 'search/clearHistories':
      return {
        ...state,
        histories: [],
      };

    case 'search/saveResults':
      return {
        ...state,
        resultData: {
          total_count: action.payload.total_count,
          per_page: action.payload.per_page,
          page: action.payload.page,
          results: [...(action.payload.results || [])],
        },
      };

    case 'search/updateResults':
      return {
        ...state,
        resultData: {
          total_count: action.payload.total_count,
          per_page: action.payload.per_page,
          page: action.payload.page,
          results: [...state.resultData.results, ...action.payload.results],
        },
      };

    default:
      return state;
  }
}
