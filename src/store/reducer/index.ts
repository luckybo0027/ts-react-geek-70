import { combineReducers } from 'redux';
import articleReducer from './article';
import channelReducer from './channel';
import commentReducer from './comment';
import searchReducer from './search';
import userReducer from './user';

const rootReducer = combineReducers({
  user: userReducer,
  channel: channelReducer,
  search: searchReducer,
  article: articleReducer,
  comment: commentReducer,
});

export default rootReducer;
