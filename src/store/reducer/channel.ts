const initialState = {
  allList: [] as ChannelType[],
  currentId: 0,

  userList: [] as ChannelType[],
};

export type ChannelType = {
  id: number;
  name: string;
};

type ActionType =
  | { type: 'channel/saveAllList'; payload: ChannelType[] }
  | { type: 'channel/saveUserList'; payload: ChannelType[] }
  | { type: 'channel/delUserChannelById'; payload: number }
  | { type: 'channel/addUserChannel'; payload: ChannelType }
  | { type: 'channel/setChannelId'; payload: number };

export default function channelReducer(
  state = initialState,
  action: ActionType
): typeof initialState {
  switch (action.type) {
    case 'channel/saveAllList':
      return {
        ...state,
        allList: [...action.payload],
      };

    case 'channel/saveUserList':
      return {
        ...state,
        userList: [...action.payload],
      };

    case 'channel/delUserChannelById':
      return {
        ...state,
        userList: state.userList.filter((item) => item.id !== action.payload),
      };
    case 'channel/addUserChannel':
      return {
        ...state,

        userList: [...state.userList, action.payload],
      };

    case 'channel/setChannelId':
      return {
        ...state,
        currentId: action.payload,
      };

    default:
      return state;
  }
}
