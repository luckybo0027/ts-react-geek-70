import { getArtilesByIdAPI } from '@/api/article';
import { submitCommentAPI } from '@/api/comment';
import { getCommentsByIdAPI } from '@/api/detail';
import { AppDispatch } from '..';
import { CommentType } from '../reducer/comment';
export const getArtilesByIdAction = (data: { channel_id?: number; timestamp?: number }) => {
  return async (dispatch: AppDispatch) => {
    const res = await getArtilesByIdAPI(data);
    dispatch({ type: 'article/saveList', payload: { ...res.data, channel_id: data.channel_id } });
  };
};

export const updateArtilesByIdAction = (data: { channel_id?: number; timestamp?: number }) => {
  return async (dispatch: AppDispatch) => {
    const res = await getArtilesByIdAPI(data);
    dispatch({
      type: 'article/updateListById',
      payload: { ...res.data, channel_id: data.channel_id },
    });
  };
};
