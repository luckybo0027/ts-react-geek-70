import { getCommentsByIdAPI, getDetailByIdAPI, ParamsType } from '@/api/detail';
import { AppDispatch } from '..';

export const getDetailByIdAction = (id: string) => {
  return async (dispatch: AppDispatch) => {
    const res = await getDetailByIdAPI(id);
    dispatch({ type: 'article/saveDetail', payload: res.data });
  };
};

export const clearArticleAction = () => {
  return {
    type: 'article/saveDetail',
    payload: {},
  };
};

export const getCommentsByIdAction = (params: ParamsType) => {
  return async (dispatch: AppDispatch) => {
    dispatch({ type: 'comment/setLoading', payload: true });
    const res = await getCommentsByIdAPI(params);
    dispatch({ type: 'comment/saveList', payload: res.data.results });
    dispatch({ type: 'comment/setLoading', payload: false });
  };
};
export const clearCommentsAction = () => {
  return {
    type: 'comment/saveList',
    payload: [],
  };
};
