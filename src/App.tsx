import React, { useCallback, useEffect, useMemo, useState } from 'react';

// 1.useCallback，
// 作用：对函数的声明进行缓存
// 💥特殊要注意的地方： []没有依赖时，会产生闭包效果，count永远是初始值
// 解决办法：
// 1。监听依赖项： [依赖项]
// 2. 使用形参避免依赖count,产生闭包问题

export default function App() {
  const [count, setCount] = useState(0);

  const handAdd = useCallback((_count) => {
    console.log('count  ----->  ', _count);
    setCount(_count + 1); // 如果空数组时，访问的count类似闭包效果，每次访问的count都是0
  }, []);
  return (
    <div>
      <Count count={count} onAdd={handAdd}></Count>
      <h1>{count}</h1>
      <hr />
      {/* <button onClick={() => setCount(count + 1)}>点我count + 1</button> */}
      <hr />
    </div>
  );
}
function Count({ count, onAdd }: any) {
  return <h1 onClick={() => onAdd(count)}>点我+1</h1>;
}
