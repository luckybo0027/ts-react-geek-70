/**
 *  学习目标：Todos -TS + Redux版 案例
 *    关键点：
*       1. 💥 使用useAppSelector获取state，跟着官网走，去cv
        2. 💥 reducer函数一定要加返回值类型
        3. 💥 ts中不解构action，解构action类型会丢失

        
  *    需求1：✅设计redux仓库
  *    需求2：✅修复state丢失问题、渲染列表
        需求3：✅删除一条数据
        需求4：✅让action对象，有代码提示
        需求5：✅清除已完成
        需求6：改造为，不解构action
        需求7：✅更新一条数据
        需求8：✅新增-受控组件
        需求9：✅新增-功能实现
        优化：需要dispatch限定参数只能为ActionType
        💥 使用useAppDispatch获取dispatch函数，跟着官网走，去cv
        需求10：✅点谁谁高亮
        需求10：✅点谁谁高亮
        需求11：✅数据切换
        需求12：✅数据缓存
        需求13：✅全选功能
          ✅全选影响小选
          ✅小选影响全选
       
   
 *
 */
import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from './store';
import { addAction, clearAllAction, delTaskByIdAction, toggleAllAction, toggleTypeAction, updateByIdAction } from './store/action/todo';
import './styles/base.css';
import './styles/index.css';

export default function App() {
  return (
    <section className="todoapp">
      {/* 头部 */}
      <Header></Header>
      {/* 主体 */}
      <Main></Main>
      {/* 底部 */}
      <Footer></Footer>
    </section>
  );
}


function Main() {
  // 3. 渲染列表
  // 11.1 获取type
  const { list, type } = useAppSelector((state) => state.todo)
  // 12.1 监听数据变化
  useEffect(() => {
    // 12. 存入缓存
    localStorage.setItem('todo-list', JSON.stringify(list))
    localStorage.setItem('todo-type', type)
  }, [list, type])


  // 3.3 diapatch调用
  const dispatch = useAppDispatch()


  // 11.2 定义计算属性
  const showList = list.filter((item) => {
    // 如果active 返回 isDone为false的
    if (type === 'active') return !item.isDone
    // 如果completed ,返回isDone为true
    if (type === 'completed') return item.isDone
    // 如果 all  返回 true
    // 🔔ESLint 检查，可以省略最后一个if条件即可
    return true
  })

  // 13.6 定义计算属性 isAll得到布尔值
  const isAll = list.length ? list.every((item) => item.isDone) : false
  console.log('isAll  ----->  ', isAll);
  // 13.7 给全选按钮绑定checked属性


  return (
    <section className="main">
      <input id="toggle-all" className="toggle-all" type="checkbox"
        // 13.4 点击全选按钮，dispatch触发
        onChange={e => dispatch(toggleAllAction(e.target.checked))}
        checked={isAll} />
      <label htmlFor="toggle-all">全选</label>
      <ul className="todo-list">
        {
          // 11.3 替换list
          showList.map((item) => {
            return <li key={item.id} className={item.isDone ? "completed" : ""}>
              <div className="view">
                {/* 7.4 dispatch触发 */}
                <input className="toggle" type="checkbox" checked={item.isDone} onChange={() => dispatch(updateByIdAction(item.id))} />
                <label>{item.task}</label>
                <button className="destroy" onClick={() => dispatch(delTaskByIdAction(item.id))}></button>
              </div>
            </li>
          })
        }
      </ul>
    </section>
  );
}


function Footer() {
  const dispatch = useAppDispatch()
  // 10.1. 获取状态
  const { type } = useAppSelector(state => state.todo)
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>1</strong> 剩余
      </span>
      <ul className="filters">
        <li>
          {/* 10.2 排它比较 */}
          <a className={type === "all" ? "selected" : ""} href="#/"
            // 10.7 点击事件dispatch触发action
            onClick={() => dispatch(toggleTypeAction('all'))}
          >
            全部
          </a>
        </li>
        <li>
          <a className={type === "active" ? "selected" : ""} href="#/active"
            // 10.7 点击事件dispatch触发action
            onClick={() => dispatch(toggleTypeAction('active'))}
          >
            未完成
          </a>
        </li>
        <li>
          <a className={type === "completed" ? "selected" : ""} href="#/completed"
            // 10.7 点击事件dispatch触发action
            onClick={() => dispatch(toggleTypeAction('completed'))}
          >
            已完成
          </a>
        </li>
      </ul>
      <button className="clear-completed" onClick={() => dispatch(clearAllAction())}>清除已完成</button>
    </footer>
  );
}

function Header() {
  // 8.1. 声明状态
  const [task, setTask] = useState('')
  const dispatch = useAppDispatch()

  const handleKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    // 3. 👍使用key，替代keyCode
    if (e.key === 'Enter') {
      console.log('回车被触发了  ----->  ',);
      // 9.4 dispatch触发
      dispatch(addAction(task))
      setTask('')

    }
  }
  return (
    <header className="header">
      <h1>todos</h1>
      <input
        onKeyDown={e => handleKeyDown(e)}
        className="new-todo" placeholder="需要做什么" autoFocus value={task}
        // 2.
        onChange={e => setTask(e.target.value)} />
    </header>
  );
}
