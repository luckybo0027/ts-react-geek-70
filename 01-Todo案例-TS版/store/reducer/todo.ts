type TodoItemType = {
  isDone: boolean;
  task: string;
  id: number;
};
const initialState = {
  // 12.3 设置reducer中的初始值，从缓存中取出
  // 🔔 记得处理为null的情况，和类型断言的情况
  list: JSON.parse(localStorage.getItem('todo-list') || '[]') as TodoItemType[],
  // 10.5 适用断言，保证类型准确
  type: (localStorage.getItem('todo-type') || 'all') as ToggleType, // all active completed
};

export type ToggleType = 'all' | 'active' | 'completed';

// 4.1. 定义Action的类型
export type ActionType =
  // 5.1 扩展ActionType的类型， 字面量+联合类型
  | { type: 'todo/del'; payload: number }
  | { type: 'todo/clear' }
  // 7.1 扩展action类型
  | { type: 'todo/update'; payload: number }
  // 9.1  扩展ActionTYpe的类型
  | { type: 'todo/add'; payload: { id: number; task: string; isDone: boolean } }
  // 10.3 扩展ActionTYpe的类型
  | { type: 'todo/updateType'; payload: ToggleType }
  // 13.1 扩展ActionTYpe的类型
  | { type: 'todo/toggelAll'; payload: boolean };

// 4.2.把ActionType 指定给Action对象
// 6.1 不解构action
export default function todoReducer(state = initialState, action: ActionType): typeof initialState {
  // 6.2 打印action，不打印action.payload
  console.log('进入仓库了  ----->  ', action);
  switch (action.type) {
    // 3.1 定义删除逻辑分支
    case 'todo/del':
      return {
        ...state,
        // 3.4 删除逻辑完成
        list: state.list.filter((item) => item.id !== action.payload),
      };
    // 5.2 定义分支逻辑
    case 'todo/clear':
      return {
        ...state,
        // 5.5 完成清除的功能
        list: state.list.filter((item) => !item.isDone),
      };
    // 7.2 新增逻辑
    case 'todo/update':
      return {
        ...state,
        // 7.5 更新的逻辑
        list: state.list.map((item) => ({
          ...item,
          isDone: item.id === action.payload ? !item.isDone : item.isDone,
        })),
      };

    // 9.2 新增reducer逻辑分支
    case 'todo/add':
      return {
        ...state,
        list: [...state.list, action.payload],
      };

    // 10.4 定义分支，编写逻辑
    case 'todo/updateType':
      return {
        ...state,
        type: action.payload,
      };

    // 13.2 新增逻辑分支
    case 'todo/toggelAll':
      return {
        ...state,
        /* 13.5 完成全选的计算逻辑*/
        list: state.list.map((item) => ({
          ...item,
          isDone: action.payload,
        })),
      };


    default:
      return state;
  }
}
