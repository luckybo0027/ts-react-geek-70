// 3.2 创建action函数
import { ActionType, ToggleType } from '../reducer/todo';
// 4.3. 给Action创建函数，指定返回值类型是ActionType
export const delTaskByIdAction = (id: number): ActionType => {
  return {
    type: 'todo/del',
    payload: id,
  };
};

// 5.3 清除的action创建函数
export const clearAllAction = (): ActionType => {
  return {
    type: 'todo/clear',
    // 6.3 删掉acton.payload键
  };
};
// 7.3 action
export const updateByIdAction = (id: number): ActionType => {
  return {
    type: 'todo/update',
    payload: id,
    // 6.3 删掉acton.payload键
  };
};

// 9.3 创建Action函数
export const addAction = (task: string): ActionType => {
  return {
    type: 'todo/add',
    payload: { id: Date.now(), isDone: false, task },
  };
};

// 10.6 创建action函数
export const toggleTypeAction = (type: ToggleType): ActionType => {
  return {
    type: 'todo/updateType',
    payload: type,
  };
};

// 13.3 创建Action函数
export const toggleAllAction = (checked: boolean): ActionType => {
  return {
    type: 'todo/toggelAll',
    payload: checked,
  };
};


