import React from 'react';
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import App from './App';
import store from './store'

// 连接Redux
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root'))